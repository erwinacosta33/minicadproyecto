package Minicad;

import java.awt.Color;
import java.awt.Point;


/**
 *
 * @author erwin
 */
public class Figura {

    Color color;
    Point puntos[];
    int id;
    double d;

    public Figura(Point punto[], int id,double d, Color c) {
        puntos = new Point[punto.length];
        for (int i = 0; i < puntos.length; i++) {
            this.puntos[i] = new Point(punto[i].x,punto[i].y);
        }
        this.id = id;
        this.color = c;
        this.d= d;
    }

    public Figura() {
    }
}
